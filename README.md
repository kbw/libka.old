ka, a C/C++ utility library.
by Keith and Antonello

There are not many help libraries around for C++ of
sufficient quality.  We want stuff that's:
-	fast
-	efficient
-	has clean interfaces
-	elegantly written
-	portable across POSIX
-	useful
-	missing elsewhere

The biggest pain point with C++ is the lack of general
purpose libraries. Everyone needa configuration, logging,
..., yet there's no good solution in C++. It's like
building a dog from quarks.

It contains
-	my own C zero copy string routines
-	my portable timer (Windows, Mac, Linux/FreeBSD)
-	Ruby's CSV parser

wip:
-	Yii's logger
-	Python's numpy
