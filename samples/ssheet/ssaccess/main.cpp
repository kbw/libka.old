#include "process.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

typedef std::vector<const char*> cstrings_type;

struct options
{
	char delimiter;
	cstrings_type files;

	options();
};
options get_options(int argc, char* argv[]);

int main(int argc, char* argv[])
{
	options opts = get_options(argc, argv);

	if (opts.files.empty())
	{
		process(std::cin, opts.delimiter);
	}
	for (auto file : opts.files)
	{
		struct stat info;
		stat(file, &info);
		fprintf(stderr, "%s:%d\n", file, (int)info.st_size);
		std::ifstream is(file);
		process(is, opts.delimiter);
	}
}

options::options() :
	delimiter(',')
{
}

options get_options(int argc, char* argv[])
{
	options opts;

	for (int i = 1; i < argc; ++i)
	{
		if (strcmp(argv[i], "-d") == 0 &&
			(i + 1) < argc &&
			argv[i + 1][0] != '-' &&
			strlen(argv[i + 1]) == 1)
		{
			opts.delimiter = argv[i + 1][0];
			++i;
		}
		else if (argv[i][0] != '-')
		{
			opts.files.push_back(argv[i]);
		}
	}

	return opts;
}
