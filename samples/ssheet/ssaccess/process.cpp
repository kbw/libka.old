#include "process.hpp"
#include <ka/util/space.hpp>
#include <ka/core/time.hpp>
#include <istream>
#include <fstream>

#include <iostream>
#include <iomanip>
#include <algorithm>
//#include <stdio.h>

void process(std::istream& is, char delimiter)
{
	ka::core::HRTime start;
	hrtime_t dt;

	fprintf(stderr, "loading sheet ...\n");
	start = ka::core::HRTime::now();
	ka::util::space::sheet sheet(is, delimiter);
//	if (!sheet.empty())
//		;
	dt = (start - ka::core::HRTime::now());
//	fprintf(stderr, "%ld.%09ld\n", dt.tv_sec, dt.tv_nsec);
	std::clog << dt << std::endl;

	fprintf(stderr, "append to kw.csv\n");
	start = ka::core::HRTime::now();
	std::ofstream os("kw.csv", std::ios::app);
	sheet.saveOn(os);
	dt = (start - ka::core::HRTime::now());
//	fprintf(stderr, "%ld.%09ld\n", dt.tv_sec, dt.tv_nsec);
	std::clog << dt << std::endl;

	// test indexing
	fprintf(stderr, "test indexing ...\n");
	start = ka::core::HRTime::now();
	size_t rows = sheet.nrows();
	size_t cols = 0;

	for (size_t i = 0; i != rows; ++i)
		cols = std::max(cols, sheet.ncols(i));
	dt = (start - ka::core::HRTime::now());
//	fprintf(stderr, "dt=%ld.%09ld\n", dt.tv_sec, dt.tv_nsec);
//	std::clog << "dt=" << dt.tv_sec << "." << std::setw(9) << std::setfill('0') << dt.tv_nsec << "\n";
	std::clog << dt << std::endl;
//	fprintf(stderr, "%lu/%lu\n", cols, rows);
	std::clog << cols << "/" << rows << "\n";

	std::string idx = "$AA23";
//	fprintf(stderr, "coord=%s\n", idx.c_str());
	std::clog << "coord=" << idx << "\n";
	start = ka::core::HRTime::now();
	sheet[idx];
	dt = (start - ka::core::HRTime::now());
//	std::clog << "dt=" << dt.tv_sec << "." << std::setw(9) << std::setfill('0') << dt.tv_nsec << "\n";
	std::clog << dt << std::endl;

	idx = "$ZZ$230";
//	fprintf(stderr, "coord=%s\n", idx.c_str());
	std::clog << "coord=" << idx << "\n";
	start = ka::core::HRTime::now();
	sheet[idx];
	dt = (start - ka::core::HRTime::now());
//	std::clog << "dt=" << dt.tv_sec << "." << std::setw(9) << std::setfill('0') << dt.tv_nsec << "\n";
	std::clog << dt << std::endl;
}
