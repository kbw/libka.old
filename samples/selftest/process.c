#include "process.h"
#include <dlfcn.h>
#include <stdio.h>

static selftest_func* dl_selftest_func(void* h, const char* name)
{
	/* remove warning: sym is data, func is code. idea taken from FreeBSD dlfunc */
	union
	{
		void* sym;
		selftest_func* func;
	} ret;

	ret.sym = dlsym(h, name);
	return ret.func;
}

void process(const char* filename)
{
	void* h = dlopen(filename, RTLD_NOW);
	if (h)
	{
		selftest_func* func = dl_selftest_func(h, "self_test");
		if (func)
		{
			puts(filename);
			func();
		}

		dlclose(h);
	}
}
