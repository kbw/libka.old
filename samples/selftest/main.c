#include "process.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage();
int is_directory(const char* entry);
int is_file(const char* entry);
void traverse(const char* path, void (*process)(const char*));

int main(int argc, char* argv[])
{
	if (argc == 1)
	{
		usage();
		return 0;
	}

	int i;
	for (i = 1; i < argc; ++i)
	{
		if (is_directory(argv[i]))
		{
			traverse(argv[i], process);
		}
		else if (is_file(argv[i]))
		{
			process(argv[i]);
		}
	}

	return 0;
}

void usage()
{
	static const char* msg[] =
	{
		"selftest: executes the selftest functions within libraries",
		"usage:",
		"\tselftest filename/directory ...",
		NULL
	};

	int i;
	for (i = 0; msg[i]; ++i)
		fprintf(stderr, "%s\n", msg[i]);
}

int is_directory(const char* entry)
{
	struct stat info;
	int err = stat(entry, &info);

	return (err == 0) && (info.st_mode & S_IFDIR);
}

int is_file(const char* entry)
{
	struct stat info;
	int err = stat(entry, &info);

	return (err == 0) && (info.st_mode & S_IFREG);
}

void traverse(const char* path, void (*process)(const char*))
{
	DIR* dir = opendir(path);
	if (dir)
	{
		struct dirent* entry;
		while ( (entry = readdir(dir)) )
		{
			if (entry->d_name[0] == '.')
				continue;

			char* path2 = NULL;
			asprintf(&path2, "%s/%s", path, entry->d_name);

			if (entry->d_type & DT_REG)
			{
				process(path2);
			}
			if (entry->d_type & DT_DIR)
			{
				traverse(path2, process);
			}

			free(path2);
		}
		
		closedir(dir);
	}
}
