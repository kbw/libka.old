#include <ka/util/space.hpp>

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdio.h>

void saveOn(std::string name, const ka::util::space::sheet& csv, char de = ',');

int main(int argc, char* argv[])
{
	ka::util::space::sheet ss;
	for (int r = 0; r < argc; ++r)
	{
		for (int c = 0; c < argc; ++c)
		{
			char idx[16];
			snprintf(idx, sizeof(idx), "%c%d", 'A' + c, r + 1);

			ss[idx] = ka::util::space::cell_type(
						argv[c], argv[c] + strlen(argv[c]));
		}
	}

	saveOn("a.csv", ss);
}

void saveOn(std::string name, const ka::util::space::sheet& csv, char de)
{
	std::ofstream os(name);
	csv.saveOn(os, de);
}
