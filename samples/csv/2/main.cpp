#include <ka/yii/log.hpp>
#include <ka/util/space.hpp>
#include <ka/core/defs.hpp>
#include <ka/core/time.hpp>
#include <ka/core/util.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>

void process(ka::util::space::sheet& ss);
void saveOn(std::string name, const ka::util::space::sheet& csv, char de = ',');

int main(int argc, char* argv[])
{
	for (int i = 1; i < argc; ++i)
	{
		ka::core::HRTimer t;
		ka::util::space::sheet ss;
		{
			std::ifstream is(argv[i]);
			ss.foreach(is);
		}

		LOG_INFO
			<< t.reset()
			<< " foreach:" << argv[i] << ' '
			<< ss.read_time() << ' '
			<< ss.process_time() << ' '
			<< ss.parse_time() << ' '
			<< ss.row_add_time() << ' '
			<< std::endl;

		t.start();
		process(ss);
		LOG_INFO
			<< t.reset()
			<< " process" << std::endl;

		t.start();
		saveOn("a.csv", ss);
		LOG_INFO
			<< t.reset()
			<< " saveOn" << std::endl;
	}
	if (argc == 1)
	{
		ka::core::HRTimer t;
		ka::util::space::sheet ss;

		ss.foreach(std::cin);
		LOG_INFO
			<< t.reset()
			<< " foreach: <stdin>" << std::endl;

		t.start();
		process(ss);
		LOG_INFO
			<< t.reset()
			<< " process" << std::endl;

		t.start();
		saveOn("a.csv", ss);
		LOG_INFO
			<< t.reset()
			<< " saveOn" << std::endl;
	}
}

void process(ka::util::space::sheet& ss)
{
	size_t mxcol = 0;
	for (size_t row = 0; row != ss.nrows(); ++row)
		mxcol = std::max(mxcol, ss.ncols(row));
	LOG_INFO
		<< "mx row=" << ss.nrows()
		<< ", mx col=" << mxcol << std::endl;

	auto p = ss[0].store("ka");
	ka::util::space::cell c(p->c_str(), p->c_str() + p->size());
	for (size_t row = 1; row < ss.nrows(); ++row)
		ss[row].set(mxcol, c);
}

void saveOn(std::string name, const ka::util::space::sheet& csv, char de)
{
	std::ofstream os(name);
	csv.saveOn(os, de);
}
