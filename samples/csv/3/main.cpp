#include <ka/gem/CSV/CSV.hpp>
#include <fstream>
#include <iostream>

int main(int argc, char* argv[])
{
	ka::gem::CSV ss;
	for (int i = 1; i < argc; ++i)
	{
		std::ifstream is(argv[i]);
		ss.foreach(is);
	}

//	ka::gem::CSV raw_values = select(ss, { 3, 5, 6 });
//	ka::gem::CSV norm_values = norm( raw_values );
	ka::gem::CSV n = norm( ss );
	ss.saveOn(std::cout);
	n.saveOn(std::cout);
}
