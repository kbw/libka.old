SHELL    := $(shell which sh)
PLATFORM := $(shell uname)

ifeq "$(STD)" ""
CFLAGS   += -std=c11
else
CFLAGS   += -std=$(STD)
endif
CFLAGS   += -pedantic

ifeq "$(CXXSTD)" ""
CXXFLAGS += -std=c++11
else
CXXFLAGS += -std=$(CXXSTD)
endif
CXXFLAGS += -pedantic

ifeq ($(strip $(RELEASE)), true)
CFLAGS   += -O3 -D_FORTIFY_SOURCE=2 -DNDEBUG  -fstack-protector-all -Wstack-protector
CXXFLAGS += -O3 -D_FORTIFY_SOURCE=2 -DNDEBUG  -fstack-protector-all -Wstack-protector
else
CFLAGS   += -g -DSELF_TEST
CXXFLAGS += -g -DSELF_TEST
endif

ifdef KA_NOICU
CXXFLAGS += -DKA_NOICU=1
endif

ifeq ($(strip $(TYPE)), LIB)
  ifeq ($(strip $(PLATFORM)), Darwin)
LDFLAGS  += -dynamics
  else
CFLAGS   += -fPIC -DPIC
CXXFLAGS += -fPIC -DPIC
LDFLAGS  += -shared
  endif
endif

LDFLAGS  += -Wl,-x -Wl,--fatal-warnings -Wl,--warn-shared-textrel
ifeq ($(strip $(PLATFORM)), Linux)
LDFLAGS  += -ldl
endif

# dependency files: .depend
%.depend: %.c
	$(COMPILE.c) -MM $< >> $@

%.depend: %.cc
	$(COMPILE.cc) -MM $< >> $@

%.depend: %.cpp
	$(COMPILE.cpp) -MM $< >> $@

%.depend: %.C
	$(COMPILE.C) -MM $< >> $@
