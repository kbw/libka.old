PREFIX   ?= /usr/local

#CFLAGS  += -D_XOPEN_SOURCE=700
#CFLAGS  += -O2
CFLAGS   += -g
CFLAGS   += -D_GNU_SOURCE=1
CFLAGS   += -Wall -Wextra -Wconversion -Wno-unused-parameter
CFLAGS   += -I.. -I../..
CFLAGS   += -isystem $(PREFIX)/include

#CXXFLAGS += -D_XOPEN_SOURCE=700
#CXXFLAGS += -O2
CXXFLAGS += -g
CXXFLAGS += -D_GNU_SOURCE=1
CXXFLAGS += -Wall -Wextra -Wconversion -Wno-unused-parameter
CXXFLAGS += -I..
CXXFLAGS += -isystem $(PREFIX)/include
