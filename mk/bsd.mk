SHELL    := /bin/sh
PLATFORM := $(shell uname)

.if !defined(CSTD)
CFLAGS   += -std=c11
.else
CFLAGS   += -std=$(CSTD)
.endif
CFLAGS   += -pedantic

.if !defined(CXXSTD)
CXXFLAGS += -std=c++11
.else
CXXFLAGS += -std=$(CXXSTD)
.endif
CXXFLAGS += -pedantic

CXXFLAGS += -Wall -Wextra -Wconversion

.if defined(RELEASE) && $(RELEASE) == "true"
CFLAGS   += -O3 -D_FORTIFY_SOURCE=2 -NDEBUG  -fstack-protector-all -Wstack-protector
CXXFLAGS += -O3 -D_FORTIFY_SOURCE=2 -NDEBUG  -fstack-protector-all -Wstack-protector
.else
CFLAGS   += -g -DSELF_TEST
CXXFLAGS += -g -DSELF_TEST
.endif

.if defined(KA_NOICU)
CXXFLAGS += -DKA_NOICU=1
.endif

.include "common.mk"
