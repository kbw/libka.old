#pragma once

#include "logfwd.hpp"
#include "../core/defs.hpp"
#include <map>

namespace ka
{
	namespace yii
	{
		// CLogRouter manages log routes that record log messages in
		// different media.
		class LogRouter
		{
		public:
			typedef std::pair<basic_string, LogRoute*> Route;
			typedef std::multimap<basic_string, LogRoute*> Routes;

			LogRouter(std::istream& config);
			LogRouter(const basic_string& config);

			const Routes& getRoutes() const;

		protected:
			void addRoute(Route& route);

		private:
			Routes routes;
		};

		// A log route object retrieves log messages from a logger and
		// sends it somewhere, such as files, emails. The messages being
		// retrieved may be filtered first before being sent to the
		// destination.
		// The filters include log level filter and log category filter.
		class LogRoute
		{
		public:
			virtual	~LogRoute();

			static LogRoute* create(basic_string name);

		private:
			bool enabled;
			LogRoute* filter;
			basic_strings categories;
			basic_strings except;
			basic_strings levels;
			basic_strings logs;
		};

		class LogStream
		{
		public:
			void init(std::istream& config);
			void init(const basic_string& config);

			LogStream&	operator<<(const basic_string& val);
			LogStream&	operator<<(const char* val);
			LogStream&	operator<<(int val);

		private:
			static LogRouter*	router;
		};

		extern LogStream trace;
		extern LogStream log;
		extern LogStream err;
	}
}
