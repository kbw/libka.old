#include "loghelper.hpp"
#include <stdexcept>

namespace ka
{
	namespace yii
	{
		LogStream trace;
		LogStream log;
		LogStream err;

		// CLogRouter manages log routes that record log messages in
		// different media.
		LogRouter::LogRouter(std::istream& config)
		{
		}

		LogRouter::LogRouter(const basic_string& config)
		{
		}

		const LogRouter::Routes& LogRouter::getRoutes() const
		{
			return routes;
		}

		void LogRouter::addRoute(Route& route)
		{
			routes.insert(route);
		}

		// A log route object retrieves log messages from a logger and
		// sends it somewhere, such as files, emails. The messages being
		// retrieved may be filtered first before being sent to the
		// destination.
		// The filters include log level filter and log category filter.
		LogRoute::~LogRoute()
		{
		}

		LogRoute* LogRoute::create(basic_string name)
		{
			if (name == "syslog")	return new SyslogLogRoute();
			if (name == "file")		return new FileLogRoute("/tmp/file.log");
			if (name == "email")	return new EmailLogRoute("keith.dev.uk@gmail.com");

			throw std::invalid_argument(name);
		}

/*
			bool enabled;
			LogRoute* filter;
			basic_strings categories;
			basic_strings except;
			basic_strings levels;
			basic_strings logs;
 */
		SyslogLogRoute::SyslogLogRoute()
		{
		}

		FileLogRoute::FileLogRoute(string filename)
		{
		}

		EmailLogRoute::EmailLogRoute(string)
		{
		}

		void LogStream::init(std::istream& config)
		{
		}

		void LogStream::init(const basic_string& config)
		{
		}

		LogStream& LogStream::operator<<(const basic_string& val)
		{
			return *this;
		}

		LogStream& LogStream::operator<<(const char* val)
		{
			return *this;
		}

		LogStream& LogStream::operator<<(int val)
		{
			return *this;
		}
	}
}
