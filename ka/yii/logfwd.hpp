#pragma once

namespace ka
{
	namespace yii
	{
		class LogRouter;
		class SyslogLogRoute;
		class LogRoute;
		class FileLogRoute;
		class EmailLogRoute;
		class LogStream;
	}
}
