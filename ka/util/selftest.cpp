#ifdef SELF_TEST

#include "space.hpp"
#include <ka/core/strref.hpp>
#include <sstream>
#include <stdio.h>

void CSV_tests()
{
	const char* csv[] =
	{
		"2016-06-24, 0857, 23, 17, 29, 31\n",
		"apple\n",
		"\n",
		"1,2,3\n",
		"\"Junior\"\\, \"Horatio\"\\, \"Keith\",\"Kenny\"\\, \"Ewan\"\n",
		nullptr
	};
	std::stringstream ss;
	for (int i = 0; csv[i]; ++i)
		ss << csv[i];

	ka::util::space::sheet t1(ss);
	(t1.nrows() == 5)
		? puts("passed") : printf("# rows == %zu\n", t1.nrows());

	(t1.ncols(0) == 6)
		? puts("passed") : printf(" ncols(0) == %zu\n", t1.ncols(0));

	(t1.ncols(1) == 1)
		? puts("passed") : printf(" ncols(1) == %zu\n", t1.ncols(1));

	(t1.ncols(2) == 0)
		? puts("passed") : printf(" ncols(2) == %zu\n", t1.ncols(2));

	(t1.ncols(3) == 3)
		? puts("passed") : printf(" ncols(3) == %zu\n", t1.ncols(3));

	(t1.ncols(4) == 2)
		? puts("passed") : printf(" ncols(4) == %zu\n", t1.ncols(4));

	try
	{
		(t1.ncols(5) == 0)
			? puts(" index 5 out of range")
			: puts(" index 5 out of range");
	}
	catch (const std::exception &)
	{
		puts("passed");
	}

	ka::util::space::sheet rec(ss);
}

extern "C" void self_test(void)
{
	CSV_tests();
}

#endif
