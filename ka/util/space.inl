#pragma once

#include "ka/core/util.hpp"
#include <istream>
#include <utility>
#include <stdlib.h>

namespace ka
{
	inline error::error(const std::string& msg) :
		std::runtime_error(msg)
	{
	}

	namespace util
	{
		inline error::error(const std::string& msg) :
			ka::error(msg)
		{
		}

		namespace space
		{
			inline error::error(const std::string& msg) :
				ka::util::error(msg)
			{
			}

			inline bad_value::bad_value(const std::string& msg) :
				error(msg)
			{
			}

			inline bad_index::bad_index(const std::string& msg) :
				error(msg)
			{
			}

			// ---------------------------------------------------------------
			//
			inline cell::cell(const struct substr& n) :
				tag(etag::ezcstr)
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): " << (int)tag << ": struct substr\n";
				substr_init(&val.ref, n.begin, n.end);
			}

			inline cell::cell(const core::strref& n) :
				tag(etag::ezcstr)
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): " << (int)tag << ": core::strref\n";
				substr_init(&val.ref, n.begin(), n.end());
			}

			inline cell::cell(const char* begin, const char* end) :
				tag(etag::estr)
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): " << (int)tag << ": const cha*, const char*\n";
				val.str = new std::string(begin, end);
			}

			inline cell::cell(const std::string& n) :
				tag(etag::estr)
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): " << (int)tag << ": std::string\n";
				val.str = new std::string(n);
			}

			inline cell::cell(double n) :
				tag(etag::edouble)
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): " << (int)tag << ": double\n";
				val.dval = n;
			}

			inline cell::cell(int64_t n) :
				tag(etag::eint)
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): " << (int)tag << ": int\n";
				val.ival = n;
			}

			inline cell::cell() :
				tag(etag::eint)
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): " << (int)tag << ": default\n";
				val.ival = 0;
			}

			inline cell::~cell()
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): " << (int)tag << "\n";
				clear();
			}

			// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			//
			inline cell::cell(const cell& n) :
				tag(etag::eint)
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): copy (" << (int)n.tag << "->" << (int)tag << ")\n";
				*this = n;
			}

			inline cell& cell::operator=(const cell& n)
			{
				if (this != &n)
				{
					clear();

					tag = n.tag;
					switch (tag)
					{
					case etag::ezcstr:
						val.ref = n.val.ref;
						break;
					case etag::estr:
						val.str = new std::string(*n.val.str);
						break;
					case etag::edouble:
						val.dval = n.val.dval;
						break;
					case etag::eint:
						val.ival = n.val.ival;
						break;

					default:
						;
					}
				}

				return *this;
			}

			// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			//
			inline cell::cell(cell&& n) :
				tag(n.tag),
				val(n.val)
			{
				//std::cout << "cell::" << __FUNCTION__ << "(" << (void*)this << "): move (" << (int)tag << ")\n";
				n.tag = etag::eunset;
				n.val.ival = 0;
			}

			inline cell& cell::operator=(cell&& n)
			{
				if (this != &n)
				{
					tag = n.tag;
					val = n.val;

					n.tag = etag::eunset;
					n.val.ival = 0;
				}

				return *this;
			}

			// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			//
			inline void cell::assign(const char* begin, const char* end)
			{
				clear();

				tag = etag::estr;
				val.str = new std::string(begin, end);
			}

			inline core::strref cell::get_strref() const
			{
				if (tag == etag::ezcstr)
					return core::strref(val.ref);

				throw bad_value(
						ka::format(
							"get_strref: has %s",
							(tag == etag::ezcstr)	? "strref" :
							(tag == etag::estr)		? "string" :
							(tag == etag::edouble)	? "double" :
							(tag == etag::eint)		? "int"	: "unknown"));
			}

			inline std::string cell::get_string() const
			{
				if (tag == etag::ezcstr)
					return std::string(val.ref.begin, val.ref.end);

				if (tag == etag::estr)
					return *val.str;

				throw bad_value(
						ka::format(
							"get_strref: has %s",
							(tag == etag::ezcstr)	? "strref" :
							(tag == etag::estr)		? "string" :
							(tag == etag::edouble)	? "double" :
							(tag == etag::eint)		? "int"	: "unknown"));
			}

			inline double cell::get_double() const
			{
				if (tag == etag::edouble)
					return val.dval;

				throw bad_value(
						ka::format(
							"get_strref: has %s",
							(tag == etag::ezcstr)	? "strref" :
							(tag == etag::estr)		? "string" :
							(tag == etag::edouble)	? "double" :
							(tag == etag::eint)		? "int"	: "unknown"));
			}

			inline int64_t cell::get_int() const
			{
				if (tag == etag::eint)
					return val.ival;

				throw bad_value(
						ka::format(
							"get_strref: has %s",
							(tag == etag::ezcstr)	? "strref" :
							(tag == etag::estr)		? "string" :
							(tag == etag::edouble)	? "double" :
							(tag == etag::eint)		? "int"	: "unknown"));
			}

			inline std::string cell::get_type_as_string() const
			{
				if (isstring())
					return "string";
				else if (isdouble())
					return "double";
				else if (isint())
					return "int";

				return "unknown";
			}

			inline int64_t cell::get_value_as_int() const
			{
				int64_t value = 0;

				if (isstring())
				{
					char* e = nullptr;
					value = strtoll(get_string().c_str(), &e, 10);
				}
				else if (isdouble())
					value = static_cast<int64_t>(get_double());
				else if (isint())
					value = get_int();

				return value;
			}

			inline double cell::get_value_as_double() const
			{
				double value = 0.0;

				if (isstring())
				{
					char* e = nullptr;
					value = strtod(get_string().c_str(), &e);
				}
				else if (isdouble())
					value = get_double();
				else if (isint())
					value = static_cast<double>(get_int());

				return value;
			}

			inline std::string cell::get_value_as_string() const
			{
				std::string value;

				if (isstring())
				{
					value = get_string().c_str();
				}
				else if (isdouble())
					value = std::to_string(get_double());
				else if (isint())
					value = std::to_string(get_int());

				return value;
			}
		
			// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			//
			inline bool cell::isnumeric() const
			{
				switch (tag)
				{
				case etag::eint:
				case etag::edouble:
					return true;

				case etag::ezcstr:
				case etag::estr:
				default:
					return false;
				}
			}

			inline bool cell::isformula() const
			{
				return isstring() &&
					(((tag == etag::ezcstr) && !substr_empty(&val.ref) && (*val.ref.begin    == '=')) ||
					 ((tag == etag::estr)   && !val.str->empty()       && (*val.str->begin() == '=')));
			}

			inline bool cell::isstring() const
			{
				switch (tag)
				{
				case etag::ezcstr:
				case etag::estr:
					return true;

				case etag::edouble:
				case etag::eint:
				default:
					return false;
				}
			}

			inline bool cell::isdouble() const
			{
				switch (tag)
				{
				case etag::edouble:
					return true;

				case etag::ezcstr:
				case etag::estr:
				case etag::eint:
				default:
					return false;
				}
			}

			inline bool cell::isint() const
			{
				switch (tag)
				{
				case etag::eint:
					return true;

				case etag::ezcstr:
				case etag::estr:
				case etag::edouble:
				default:
					return false;
				}
			}

			inline void cell::clear()
			{
				switch (tag)
				{
				case etag::estr:
					delete val.str;
					break;

				default:
					;
				}
			}

			// ---------------------------------------------------------------

			inline const cell_type& row::get(size_t i) const
			{
				return at(i);
			}

			inline void row::set(size_t i, std::string& val)
			{
				auto p = store(val);

				cell c(p->c_str(), p->c_str() + p->size());
				set(i, c);
			}

			inline void row::set(size_t i, const cell_type& ref)
			{
//				LOG_INFO << "i=" << i << " size=" << size() << "\n";
				if (i >= size())
					resize(i + 1);

//				LOG_INFO << "i=" << i << " size=" << size() << "\n";
				at(i) = ref;

//				LOG_INFO << "i=" << i << " size=" << size() << "\n";
			}

			inline void row::add(const cell_type& n)
			{
				push_back(n);
			}

			inline void row::add(const struct substr& n)
			{
				push_back(n);
			}

			inline void row::add(const core::strref& n)
			{
				push_back(n);
			}

			inline void row::add(const std::string& n)
			{
				push_back(n);
			}

			inline void row::add(double n)
			{
				push_back(n);
			}

			inline void row::add(int64_t n)
			{
				push_back(n);
			}

			inline row::real_strings::iterator row::store(std::string& str)
			{
				sm_strs.emplace_front(std::move(str));
				return sm_strs.begin();
			}

			inline row::real_strings::iterator row::store(const char* str)
			{
				std::string s(str);
				return store(s);
			}

			inline void row::start_timer()
			{
				m_dt = ka::core::HRTime::now();
			}

			inline void row::stop_timer()
			{
				m_dt = ka::core::HRTime::now() - m_dt;
			}

			inline ka::core::HRTime row::time() const
			{
				return m_dt;
			}

			inline ka::core::HRTime row::row_add_time() const
			{
				return m_row_add_time;
			}

			inline void row::add_row_add_time(ka::core::HRTime dt)
			{
				m_row_add_time += dt;
			}

			// ---------------------------------------------------------------

			inline sheet::index::index(sheet& obj, const std::string& idx) :
				m_obj(obj),
				m_idx(idx)
			{
			}

			inline sheet::index::operator cell_type ()
			{
				size_t row, col;
				stringToIndex(m_idx, row, col);

				if (row < m_obj.nrows() && col < m_obj.ncols(row))
					return m_obj[row][col];

				return cell_type();
			}

			inline cell_type& sheet::index::operator=(const cell_type& n)
			{
				size_t row, col;
				stringToIndex(m_idx, row, col);

				for (size_t i = m_obj.nrows(); i <= row; ++i)
					m_obj.m_idx.push_back(
						m_obj.insert(m_obj.end(), row_type()));

				row_type& therow = *m_obj.m_idx.at(row);
				for (size_t i = m_obj.ncols(row); i <= col; ++i)
					therow.add(cell_type());

				return therow[col] = n;
			}

			// ---------------------------------------------------------------

			inline sheet::sheet(std::istream& is, char delimiter)
			{
				foreach(is, delimiter);
			}

/*
			template <typename U>
			inline sheet::sheet(std::istream& is, U func, char delimiter)
			{
				foreach(is, func, delimiter);
			}

			template <typename U>
			inline U sheet::foreach(std::istream& is, U func, char delimiter)
			{
				ka::core::HRTime start(ka::core::HRTime::now());

				std::string line;
				while (getline(is, line))
				{
					row_type row{ parse(line, delimiter) };
					if (func)
						func(row);
					m_idx.push_back(insert(end(), row));
				}

				m_dt += start - ka::core::HRTime::now();
				return func;
			}
 */

			inline void sheet::foreach(std::istream& is, char delimiter)
			{
				ka::core::HRTimer t;

				std::string line;
				while (getline(is, line))
				{
					m_read_time += t.reset();

					auto ret = insert(end(), parse(line, delimiter));
					m_idx.push_back(ret);

					m_process_time += t.reset();
					m_parse_time += ret->time();
					m_row_add_time += ret->row_add_time();
				}
			}

			inline sheet::index sheet::operator[](const std::string& idx)
			{
				return index(*this, idx);
			}

			inline cell_type sheet::operator[](const std::string& idx) const
			{
				size_t row, col;
				stringToIndex(idx, row, col);

				if (row < nrows() && col < ncols(row))
					return (*m_idx[row])[col];

				return cell_type();
			}

			inline const row_type& sheet::operator[](size_t i) const
			{
				return *(m_idx.at(i));
			}

			inline row_type& sheet::operator[](size_t i)
			{
				return *(m_idx.at(i));
			}

			inline size_t sheet::nrows() const
			{
				return size();
			}

			inline size_t sheet::ncols(size_t i) const
			{
				return m_idx.at(i)->size();	// range checked, but I hate it
			}

			inline ka::core::HRTime sheet::time() const
			{
				return m_read_time + m_process_time;
			}

			inline ka::core::HRTime sheet::read_time() const
			{
				return m_read_time;
			}

			inline ka::core::HRTime sheet::process_time() const
			{
				return m_process_time;
			}

			inline ka::core::HRTime sheet::parse_time() const
			{
				return m_parse_time;
			}

			inline ka::core::HRTime sheet::row_add_time() const
			{
				return m_row_add_time;
			}

			inline int sheet::colToPos(const std::string& col)
			{
				int pos = 0;
				for (char c : col)
					pos += 26*pos + int(1 + c - 'A');

				return pos;
			}

			inline int sheet::rowToPos(const std::string& row)
			{
				return atoi(row.c_str());
			}

			inline size_t sheet::posToIndex(int pos)
			{
				return (size_t)(pos - 1);
			}

			inline int sheet::indexToPos(size_t index)
			{
				return (int)(index + 1);
			}

			// ---------------------------------------------------------------

			inline std::ostream& cell::saveOn(std::ostream& os) const
			{
				if (isstring())
					//os << "\"(string): " << (int)tag << ": " << get_string() << "\"";
					os << get_string();
				else if (isdouble())
					//os << "\"(double): " << (int)tag << ": " << get_double() << "\"";
					os << get_double();
				else if (isint())
					//os << "\"(int): " << (int)tag << ": " << get_int() << "\"";
					os << get_int();
				else
					os << "???";

				return os;
			}

			inline std::ostream& row::saveOn(std::ostream& os, char delimiter) const
			{
				for (inherited::const_iterator p = begin(); p != end(); ++p)
				{
					if (p != begin())
						os << delimiter;

					p->saveOn(os);
				}

				return os;
			}

			inline std::ostream& sheet::saveOn(std::ostream& os, char delimiter) const
			{
				for (inherited::const_iterator p = begin(); p != end(); ++p)
				{
					p->saveOn(os, delimiter);
				}

				if (begin() != end())
					os << std::endl;

				return os;
			}
		}
	}
}
