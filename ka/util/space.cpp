#include "space.hpp"
#include <ka/core/util.hpp>

#include <iterator>
#include <ostream>
#include <iostream>
#include <algorithm>

#include <string.h>
#include <stdio.h>
#include <ctype.h>

namespace ka
{
	namespace util
	{
		namespace space
		{
			row::real_strings row::sm_strs;

/*
			std::ostream& cell::saveOn(std::ostream& os) const
			{
				if (isstring())
					//os << "\"(string): " << (int)tag << ": " << get_string() << "\"";
					os << get_string();
				else if (isdouble())
					//os << "\"(double): " << (int)tag << ": " << get_double() << "\"";
					os << get_double();
				else if (isint())
					//os << "\"(int): " << (int)tag << ": " << get_int() << "\"";
					os << get_int();
				else
					os << "???";

				return os;
			}

			std::ostream& row::saveOn(std::ostream& os, char delimiter) const
			{
				for (inherited::const_iterator p = begin(); p != end(); ++p)
				{
					if (p != begin())
						os << delimiter;

					p->saveOn(os);
				}

				return os;
			}

			std::ostream& sheet::saveOn(std::ostream& os, char delimiter) const
			{
				for (inherited::const_iterator p = begin(); p != end(); ++p)
				{
					p->saveOn(os, delimiter);
				}

				if (begin() != end())
					os << std::endl;

				return os;
			}
 */

			void sheet::stringToIndex(const std::string& idx, size_t& rowidx, size_t& colidx)
			{
				// extract col/row
				// idx can be L3, $L3, L$3, $L$3
				std::string col, row;
//				bool relative_col{}, relative_row{};

				bool expect_ltr = true;
				size_t i, j;
				const size_t mx = idx.size();
				for (i = 0; i != mx; ++i)
				{
					const bool isrelative = idx[i] == '$';

					if (expect_ltr)
					{
						if (isrelative)
						{
							if (i != 0)
								throw bad_index(idx);

//							relative_col = true;
							continue;
						}

						if (!isupper(idx[i]))
							throw bad_index(idx);

						for (j = i; j != mx && isupper(idx[j]); ++j)
							;
						col.assign(&idx[i], &idx[j]);
						if (col.empty())
							throw bad_index(idx);

						i = j - 1;
						expect_ltr = false;
					}
					else
					{
						if (isrelative)
						{
							if (i != j)
								throw bad_index(idx);

//							relative_row = true;
							continue;
						}

						if (!isdigit(idx[i]))
							throw bad_index(idx);

						size_t k;
						for (k = i; k != mx && isdigit(idx[k]); ++k)
							;
						row = idx.substr(i, k);
						if (col.empty())
							throw bad_index(idx);
						if (k != mx)
							throw bad_index(idx);

						i = k - 1;
					}	// expect_ltr
				}		// for

				int colnum = colToPos(col);
				int rownum = rowToPos(row);
				colidx = posToIndex(colnum);
				rowidx = posToIndex(rownum);
//				std::clog
//					<< (relative_col ? "+" : "") << col
//					<< "/"
//					<< (relative_row ? "+" : "") << row
//					<< "\n"
//					<< (relative_col ? "+" : "") << colnum
//					<< "/"
//					<< (relative_row ? "+" : "") << rownum
//					<< "\n";
//				if (rowidx < nrows() && colidx < nrows(rowidx))
//					return (*this)[rowidx][colidx];
//
//				static cell_type ret;
//				return ret;
			}

			row_type sheet::parse(std::string& str, char delimiter)
			{
				row_type row;
				row.start_timer();

				auto line = row.store(str);
				const char* p = line->c_str();
				//std::cout << __FUNCTION__ << ": " << p << "\n";
				for (const char* q, *r = p; (q = strchr(r, delimiter)); p = r = q + 1)
				{
					// is the delimiter quoted?
					if (q > p && q[-1] == '\\')
					{
						r = q + 1;
						continue;
					}

					ka::assert_except(
						line->c_str() <= p && q <= line->c_str() + line->size(),
						std::out_of_range("cell out of range"));

					ka::core::HRTimer row_add_timer;
					//std::cout << __FUNCTION__ << ": before\n";
					//row.saveOn(std::cout) << "\n";
					//std::cout << __FUNCTION__ << ": adding strref: " << std::string(p, q) << "\n";
					row.add(core::strref(p, q));
					//std::cout << __FUNCTION__ << ": after\n";
					//row.saveOn(std::cout) << "\n";
					row.add_row_add_time(row_add_timer.delta());
				}

				if (*p)
				{
					// O(1) way to get end
					const char* begin = p;
					ptrdiff_t len = (ptrdiff_t)line->size() - (p - line->c_str());
					const char* end = p + len;
					ka::assert_except(
						line->c_str() <= begin && end <= line->c_str() + line->size(),
						std::out_of_range("cell out of range at end"));

					ka::core::HRTimer row_add_timer;
					//std::cout << __FUNCTION__ << ": before\n";
					//row.saveOn(std::cout) << "\n";
					//std::cout << __FUNCTION__ << ": adding strref: " << std::string(begin, end) << "\n";
					row.push_back(core::strref(begin, end));
					//std::cout << __FUNCTION__ << ": after\n";
					//row.saveOn(std::cout) << "\n";
					row.add_row_add_time(row_add_timer.delta());
				}

				row.stop_timer();
				//for (auto p = row.begin(); p != row.end(); ++p)
				//	std::cout
				//		<< p->get_value_as_string() << "("
				//		<< p->get_type_as_string() << ") ";
				//std::cout << std::endl;

				// we should now have a sequence of string refs
				// with the string they all reference. It's a
				// zero-copy row.
				// Return using copy-elision.
				return row;
			}
		}
	}
}
