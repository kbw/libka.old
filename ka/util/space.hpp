#pragma once

//#include <ka/core/defs.hpp>
#include <ka/core/substr.h>
#include <ka/core/rcstr.hpp>
#include <ka/core/time.hpp>

#include <string>
#include <stdexcept>

#include <memory>
#include <vector>
#include <list>
#include <set>
#include <iosfwd>

namespace ka
{
	class error : public std::runtime_error
	{
	public:
		error(const std::string& what);
	};

	namespace util
	{
		class error : public ka::error
		{
		public:
			error(const std::string& what);
		};

		namespace space
		{
			class cell;
			class row;
			class sheet;
			class cube;

			class error;
			class bad_value;
			class bad_index;

			typedef cell	cell_type;
			typedef	row		row_type;
			typedef	sheet	sheet_type;
			typedef cube	cube_type;

			class error : public ka::util::error
			{
			public:
				error(const std::string& what);
			};

			class bad_value : public error
			{
			public:
				bad_value(const std::string& what);
			};

			class bad_index : public error 
			{
			public:
				bad_index(const std::string& what);
			};

			class cell
			{
			public:
				inline	cell(const struct substr& val);
				inline	cell(const core::strref& val);
				inline	cell(const char* begin, const char* end);
				inline	cell(const std::string& val);
				inline	cell(double val);
				inline	cell(int64_t val);
				inline	cell();
				inline	~cell();

				inline	cell(const cell& n);
				inline	cell& operator=(const cell& n);

				inline	cell(cell&& n);
				inline	cell& operator=(cell&& n);

				inline	void assign(const char* begin, const char* end);
				inline	core::strref	get_strref() const;
				inline	std::string		get_string() const;
				inline	double			get_double() const;
				inline	int64_t			get_int() const;

				inline	bool isnumeric() const;
				inline	bool isformula() const;
				inline	bool isstring() const;
				inline	bool isdouble() const;
				inline	bool isint() const;

				inline	std::string		get_type_as_string() const;

				inline	std::string		get_value_as_string() const;
				inline	double			get_value_as_double() const;
				inline	int64_t			get_value_as_int() const;

				inline	std::ostream&	saveOn(std::ostream& os) const;

			private:
				inline	void clear();

				enum class etag { ezcstr, estr, edouble, eint,eunset };
				union value_t
				{
					struct substr	ref;	// zcstr
					std::string*	str;	// str, formula starts with =
					double			dval;	// double
					int64_t			ival;	// int
				};

				etag	tag;
				value_t	val;
			};

			class row : public std::vector<cell_type>
			{
			public:
				typedef std::vector<cell_type> inherited;
				typedef std::list<std::string> real_strings;

				using inherited::inherited;

				inline	const cell_type& get(size_t col) const;
				inline	void	set(size_t col, std::string& val);
				inline	void	set(size_t col, const cell_type& ref);
				inline	void	add(const cell_type& n);
				inline	void	add(const struct substr& n);
				inline	void	add(const core::strref& n);
				inline	void	add(const std::string& n);
				inline	void	add(double n);
				inline	void	add(int64_t n);

				inline	real_strings::iterator store(std::string& str);
				inline	real_strings::iterator store(const char* str);

				inline	std::ostream&	saveOn(std::ostream& os, char delimiter = ',') const;

				inline	void	start_timer();
				inline	void	stop_timer();
				inline	ka::core::HRTime time() const;
				inline	ka::core::HRTime row_add_time() const;

				inline	void add_row_add_time(ka::core::HRTime dt);

			private:
				static real_strings sm_strs;
				ka::core::HRTime m_dt;	// read stream time
				ka::core::HRTime m_row_add_time;
			};

			class sheet : public std::list<row_type>
			{
			public:
				typedef std::list<row_type>		inherited;

				class index
				{
				friend sheet;

					sheet& m_obj;
					const std::string& m_idx;

				public:
					typedef	std::vector<sheet_type::iterator> idx_type;

					index(sheet& obj, const std::string& idx);
					~index() = default;

					operator cell_type();
					cell_type& operator=(const cell_type& idx);
				};


						sheet() = default;
						~sheet() = default;

				inline	sheet(std::istream& is, char delimiter = ',');
//				template <typename U>
//				inline	sheet(std::istream& is, U func, char delimiter = ',');
//				template <typename U>
//				inline	U foreach(std::istream& is, U func, char delimiter = ',');

				inline	void foreach(std::istream& is, char delimiter = ',');

				inline	std::ostream&	saveOn(std::ostream& os, char delimiter = ',') const;

						index operator[](const std::string& idx);
						cell_type operator[](const std::string& idx) const;

				inline	const row_type& operator[](size_t row) const;
				inline	row_type& operator[](size_t row);
				inline	size_t nrows() const;
				inline	size_t ncols(size_t row) const;

				inline	ka::core::HRTime time() const;
				inline	ka::core::HRTime read_time() const;
				inline	ka::core::HRTime process_time() const;
				inline	ka::core::HRTime parse_time() const;
				inline	ka::core::HRTime row_add_time() const;

			private:
				static	row_type parse(std::string& line, char delimiter);
				static	int colToPos(const std::string& col);
				static	int rowToPos(const std::string& row);
				static	int indexToPos(size_t idx);
				static	size_t posToIndex(int pos);
				static	void stringToIndex(
								const std::string& idx,
								size_t& row, size_t& col);

			private:
				index::idx_type	m_idx;	// index of iterator
				ka::core::HRTime m_read_time;
				ka::core::HRTime m_process_time;
				ka::core::HRTime m_parse_time;
				ka::core::HRTime m_row_add_time;
			};
		}
	}
}

#include "space.inl"
