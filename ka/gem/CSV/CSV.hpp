#pragma once

#include "ka/util/space.hpp"

namespace ka
{
	namespace gem
	{
		class MalformedCSVError : public std::runtime_error
		{
		public:
			MalformedCSVError(const std::string& err) : std::runtime_error(err) {}
		};

		class CSV : public ka::util::space::sheet
		{
		public:
			typedef ka::util::space::sheet	inherited;
			typedef inherited::value_type	value_type;

			using inherited::inherited;
		};

		template <typename U>
		inline CSV select(const CSV& ss, const std::vector<int>& cols, U func);

		inline CSV select(const CSV& ss, const std::vector<int>& cols);
		inline CSV norm(const CSV& ss);
	}
}

#include "CSV.inl"
