#pragma once

#include <utility>
#include <algorithm>

namespace ka
{
	namespace gem
	{
		template <typename U>
		inline CSV select(const CSV& ss, const std::vector<int>& cols, U func)
		{
			CSV out;

			std::for_each(ss.begin(), ss.end(), [&](const CSV::value_type& row)
			{
				CSV::value_type new_row;

				new_row.reserve( cols.size() );
				for (size_t i = 0; i != cols.size(); ++i)
					new_row.push_back( func(row[i]) );

				out.push_back( new_row );
			});

			return out;
		}

		inline CSV select(const CSV& ss, const std::vector<int>& cols)
		{
			CSV out;

			std::for_each(ss.begin(), ss.end(), [&](const CSV::value_type& row)
			{
				CSV::value_type new_row;

				new_row.reserve( cols.size() );
				for (size_t i = 0; i != cols.size(); ++i)
					new_row.push_back( row[i] );

				out.push_back( new_row );
			});

			return out;
		}

		// I hate lamdas, but they can be handy
		inline CSV norm(const CSV& ss)
		{
			CSV out;

			size_t irow;
			std::vector<int64_t> max(ss.size(), std::numeric_limits<int64_t>::min());
			std::vector<int64_t> min(ss.size(), std::numeric_limits<int64_t>::max());

			// find minima/maxima
			irow = 0;
			for (const CSV::value_type& row : ss)
			{
				size_t icol = 0;
				for (const CSV::value_type::value_type& cell : row)
				{
					int64_t value = 0;
					if (cell.isstring())
					{
						char* e = nullptr;
						value = strtoll(cell.get_string().c_str(), &e, 10);
					}
					else if (cell.isdouble())
						value = static_cast<int64_t>(cell.get_double());
					else if (cell.isint())
						value = cell.get_int();

					max[irow] = std::max(max[irow], value);
					min[irow] = std::min(min[irow], value);

					++icol;
				}

				++irow;
			}

			// assign value based on ratio
			irow = 0;
			for (const CSV::value_type& row : ss)
			{
				CSV::value_type new_row;

				size_t icol = 0;
				for (const CSV::value_type::value_type& cell : row)
				{
					double value = 0.0;
					if (cell.isstring())
					{
						char* e = nullptr;
						value = strtod(cell.get_string().c_str(), &e);
					}
					else if (cell.isdouble())
						value = cell.get_double();
					else if (cell.isint())
						value = static_cast<double>(cell.get_int());

					double new_cell = (value - (double)min[irow]) / (double)(max[irow] - min[irow]);
					new_row.add( new_cell );

					++icol;
				}

				out.push_back( new_row );
			}

			return out;
		}
	}
}
