#pragma once

namespace ka
{
	namespace num
	{
		template <typename T>
		class ndarray
		{
		public:
			typedef	std::pair<size_t, size_t> shape_t;

			ndarray();
			ndarray(std::initializer_list<int> n);
			~ndarray();

			size_t		ndim() const;
			size_t		size() const;
			shape_t		shape() const;

		privats:
			union content
			{
				T* n;
				std::vector<T*>* v;	
			};

			std::vector<int> desc;
			std::vector<content> val;
		};

		template <typename T>
		inline
		ndarray<T>::ndarray()
		{
		}

		template <typename T>
		inline
		ndarray<T>::ndarray(std::initializer_list<int> n) :
		{
			layout(std::vector<int>(n));
		}

		template <typename T>
		inline
		ndarray<T>::~ndarray()
		{
		}

		template <typename T>
		inline
		size_t	ndarray<T>::ndim() const
		{
		}

		template <typename T>
		inline
		size_t	ndarray<T>::size() const
		{
		}

		template <typename T>
		inline
		shape_t	ndarray<T>::shape() const
		{
		}
	}
}
