#include "strutils.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>

#define bool int
#define true 1
#define false 0

#define min(x, y) ( ((x) < (y)) ? (x) : (y))

static const char int_types[] = "diouxX";
static const char float_types[] = "aAeEfFgG";
static const char ptrin_types[] = "p";
static const char ptrout_types[] = "n";
static const char string_types[] = "sS";

// Is ch a printf format type?
bool isformat(char ch)
{
	if (strchr(int_types, ch))
		return true;

	if (strchr(float_types, ch))
		return true;

	if (strchr(ptrin_types, ch))
		return true;

	if (strchr(ptrout_types, ch))
		return true;

	if (strchr(string_types, ch))
		return true;

	return false;
}

// A callback that modifies a printf string
struct escape_callback_rec
{
	escape_callback_t* func;
	void* ctx;
};

static struct escape_callback_rec callback;

escape_callback_t* set_escape_handler(escape_callback_t* func, void* ctx)
{
	escape_callback_t* ret = callback.func;

	callback.func = func;
	callback.ctx = ctx;

	return ret;
}

// Process printf like input, but allow modification of strings
void format(char* out, size_t outsz, const char* fmt, ...)
{
	va_list ap;			// next parameter on stack
	char field[10];		// field's format string
	const char* p;		// iterator thru 'fmt'

	if (!out)
		return;
	out[0] = out[outsz - 1] = '\0';
	va_start(ap, fmt);

	// for each field in the format string
	for (p = fmt; p; )
	{
		const char* q;	// begin of field
		const char* r;	// end of field -1
		size_t delta;	// length of field

		// find next field format
		// % is escaped by '%' and '\'
		do
			q = strchr(p, '%');
		while (	q &&
				((q[1] == '%' ||
				 (p < q && q[-1] == '\\'))) );
		if (!q)
		{
			// we're done
			strncat(out, p, outsz - 1);
			return;
		}

		// copy stuff inbetween to out
		delta = min((size_t)(q - p), outsz - strlen(out) - 1);
		strncat(out, p, delta);

		// look for end of format string
		for (r = q + 1; *r && !isformat(*r); ++r)
			;
		if (!*r)
		{
			// we failed to match the end, possibly a format string error
			strncat(out, p, outsz - 1);
			return;
		}

		memset(field, 0, sizeof(field));
		strncpy(field, q, (size_t)(r - q + 1));
//		fprintf(stderr, "r=%c fmt=%s\n", *r, field);
		if (*r == 's')
		{
			// Is this a string? If so escape it
			char* buf = NULL;
			size_t buflen = (size_t)asprintf(&buf, field, va_arg(ap, char*));

			if (callback.func)
				callback.func(out, outsz, buf, buflen, callback.ctx);
			else
				strncat(out, buf, outsz - 1);
			free(buf), buf = NULL;
		}
		else if (*r == 'n')
		{
			// the arg is updated
			char* buf = NULL;
			asprintf(&buf, field, va_arg(ap, int*));
			free(buf), buf = NULL;
		}
		else
		{
			// No special formatting, but long takes two words on the stack
			bool islong = tolower(r[-1]) == 'l';
			char* buf = NULL;
			asprintf(
				&buf, field,
				(islong ? va_arg(ap, long int) : va_arg(ap, int)));

			strncat(out, buf, outsz - 1);
			free(buf), buf = NULL;
		}
//		fprintf(stderr, "r=%c p=\"%s\" field=%s\n", *r, p, field);

		p = r + 1;
	}
	va_end(ap);
}
