#pragma once
  
#include "defs.hpp"
#include <limits>
#include <stdlib.h>

namespace ka
{
	template <class T>
	struct mallocator
	{
		typedef size_t    size_type;
		typedef ptrdiff_t difference_type;
		typedef T*        pointer;
		typedef const T*  const_pointer;
		typedef T&        reference;
		typedef const T&  const_reference;
		typedef T         value_type;

		mallocator() throw()                    {}
		~mallocator() throw()                   {}
		mallocator(const mallocator&) throw()   {}

		template <class U>
		mallocator(const mallocator<U>&) throw(){}

		template <class U>
		struct rebind                           { typedef mallocator<U> other; }

		pointer address(reference x) const      { return &x; }
		const_pointer address(const_reference x) const { return &x; }

		pointer allocate(size_type s, void const * = 0)
		{
			if (s == 0) return nullptr;

			if (pointer temp = (pointer)malloc(s * sizeof(T)))
				return temp;

			throw std::bad_alloc();
		}

		void deallocate(pointer p, size_type)   { free(p); }
		size_type max_size() const throw()      { return std::numeric_limits<size_t>::max() / sizeof(T); }
		void construct(pointer p, const T& val) { new (reinterpret_cast<void*>(p)) T(val); }
		void destroy(pointer p)                 { p->~T(); }
	};
}
