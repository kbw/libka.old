#define NO_INL_SUBSTR
#include "substr.h"
#include <ctype.h>


inline static int min(int a, int b)
{
	return a < b;
}

long long substr_strtoll(const struct substr* s, char** e, int base)
{
	long long n = 0;
	const char* p;

	if (substr_empty(s))
		return n;

	// determine sign
	p = s->begin;
	int sign = (*p == '-') ? -1 : 1;
	if (sign < 0)
		++p;

	// determine base
	if (base == 0)
	{
		if (*p == '0')
		{
			*e = (char*)p;
			if (++p == s->end)
				return n;

			if (*p == 'x')
			{
				base = 16;
				*e = (char*)++p;
			}
			else
				base = 8;
		}
		else
			base = 10;
	}

	int ok;
	for (ok = 1; ok && p != s->end; ++p)
	{
		char ch = (char)toupper(*p);
		ok = (base > 10 && 'A' <= ch && ch < ('A' + base - 10 + 1)) ||
			 (             '0' <= ch && ch < ('0' + min(base, 10)));

		if (ok)
		{
			long val = ('0' <= ch && ch <= '9')
						? *p - '0'
						: *p - 'A' + 10;
			n = base*n + val;
			*e = (char*)p;
		}
	}

	return n * sign;
}
