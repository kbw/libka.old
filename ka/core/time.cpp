#include "time.hpp"

#include <istream>
#include <ostream>
#include <iomanip>

namespace ka
{
	namespace core
	{
		//-------------------------------------------------------------------
		//
/*
		std::istream& operator>>(std::istream& is, HRTime& t)
		{
			is >> t.m_t;
			return is;
		}
 */

		std::ostream& operator<<(std::ostream& os, const HRTime& t)
		{
			using namespace std;

			struct timespec ts = { long(t.m_t/TIME_NSEC), long(t.m_t%TIME_NSEC) };
			os	<< ts.tv_sec << '.'
				<< setw(9) << setfill('0') << ts.tv_nsec;
			return os;
		}

		std::istream& loadFrom(std::istream& is, HRTime& t)
		{
			is >> t.m_t;
			return is;
		}

		std::ostream& saveOn(std::ostream& os, const HRTime& t)
		{
			os << t.m_t;
			return os;
		}

		//-------------------------------------------------------------------
		//
/*
		std::istream& operator>>(std::istream& is, HRTimer& t)
		{
			is >> t.m_start >> t.m_stop;
			return is;
		}
 */

		std::ostream& operator<<(std::ostream& os, const HRTimer& t)
		{
			os << t.m_start << ' ' << t.m_stop;
			return os;
		}

		std::istream& loadFrom(std::istream& is, HRTimer& t)
		{
			loadFrom(is, t.m_start);
			loadFrom(is, t.m_stop);
			return is;
		}

		std::ostream& saveOn(std::ostream& os, const HRTimer& t)
		{
			saveOn(os, t.m_start);
			os << ' ';
			saveOn(os, t.m_stop);
			return os;
		}
	}
}
