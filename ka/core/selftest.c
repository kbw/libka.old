#ifdef SELF_TEST

#include "strutils.h"
#include <stdio.h>
#include <string.h>

void subst(char* out, size_t outsz, const char* s, size_t sz, void* ctx)
{
	if (!ctx)
	{
		strncat(out, "<+++", outsz - 1);
		strncat(out, s,      outsz - 1);
		strncat(out, "+++>", outsz - 1);
	}
}

void self_test()
{
	char out[64];
	int pos = -1;

	format(out, 64, "--%d--%ld--", 3, 4L);
	strcmp(out, "--3--4--")
		? puts(out) : puts("passed");

	format(out, 64, "--%d--%n%ld--%-10s--", 3, &pos, 4L, "12345678");
	strcmp(out, "--3--4--12345678  --")
		? puts(out) : puts("passed");

	format(out, 64, "--%d--%ld--%-10s--%6s--%-6s--%d--",
		3, 4L, "12345678", "hello", "world", pos);
	strcmp(out, "--3--4--12345678  -- hello--world --0--")
		? puts(out) : puts("passed");
}

#endif	// SELF_TEST
