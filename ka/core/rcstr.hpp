#pragma once

#include "strref.hpp"

#include <memory>
#include <string>
#include <vector>

namespace ka
{
	namespace core
	{
		// Reference Counted string
		class rcstr
		{
			typedef	std::vector<strref>	strings_type;
			typedef	std::vector<std::shared_ptr<std::string>> buffer_type;

			strings_type	strs;
			buffer_type		buf;

		public:
			typedef	strings_type::value_type value_type;
			typedef strings_type::const_iterator const_iterator;

			inline rcstr() = default;
			inline void set(size_t i, const value_type& s);
			inline void add(const value_type& s);
			inline void push_back(const value_type& s);
			inline const_iterator begin() const;
			inline const_iterator end() const;
			inline const value_type& operator[](size_t i) const;
			inline value_type& operator[](size_t i);
			inline bool empty() const;
			inline size_t size() const;
			inline void swap(std::string& s);
		};
	}
}

#include "rcstr.inl"
