#pragma once

#ifndef KA_NOICU
#include <unicode/unistr.h>
#endif
#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>

#define LOG_INFO	std::clog << "log: "
#define LOG_ERROR	std::cerr << "error: "

namespace ka
{
	typedef std::string basic_string;
	typedef std::vector<basic_string> basic_strings;
#ifndef KA_NOICU
	typedef icu::UnicodeString string;
	typedef std::vector<string> strings;
#endif

	template <typename E>
	inline void assert_except(bool expr, const E& x)
	{
		if (!expr)
		{
			LOG_ERROR << "Func:[" << __PRETTY_FUNCTION__ << "] Exception:[" << x.what() << "]\n";
			throw x;
		}
	}

	template <typename E>
	inline void alert(bool expr, const E& x)
	{
		if (!expr)
		{
			LOG_ERROR << "Func:[" << __PRETTY_FUNCTION__ << "] Exception:[" << x.what() << "]\n";
		}
	}
}

template <typename D, typename S>
inline D convert_cast(S src)
{
	D dst = static_cast<D>(src);
	ka::assert_except(static_cast<S>(dst) != src, std::runtime_error( "convert_cast<> failed"));

	return dst;
}

#ifndef KA_NOICU
template <>
inline ka::basic_string convert_cast<ka::basic_string>(ka::string src)
{
	ka::basic_string dst;
	src.toUTF8String(dst);
	return dst;
}
#endif
