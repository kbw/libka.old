#define	NO_INL_TIMER
#include "timer.h"

/*
#if defined(MACOS)
  #include <mach/mach.h>
  #include <mach/mach_time.h>
#else
  #if defined(LINUX)
    #include <sys/time.h>
  #endif

  #include <time.h>

  #define	TIME_SEC	1
  #define	TIME_MSEC	1000
  #define	TIME_USEC	1000000
  #define	TIME_NSEC	1000000000
#endif

#include <stdlib.h>			// memset on Unix
#include <string.h>			// memset on Linux

hrtime_t zero_hrtime(void)
{
#if defined(WINDOWS)
	LARGE_INTEGER val;
	memset(&val, 0, sizeof(val));
	return val;
#else
	return 0;
#endif
}

hrtime_t get_hrtime(void)
{
#if defined(MACOS)
	return mach_absolute_time();
#elif defined(WINDOWS)
	LARGE_INTEGER val;
	QueryPerformanceCounter(&val);
	return val;
#else
	struct timespec tv;
	clock_gettime(CLOCK_MONOTONIC, &tv);
	return TIME_NSEC*tv.tv_sec + tv.tv_nsec;
#endif
}

hrtime_t diff_hrtime(hrtime_t b, hrtime_t a)
{
#if defined(MACOS)
	static mach_timebase_info_data_t ref;
	if (ref.denom == 0)
	{
		mach_absolute_info(&ref);

		while (((int)ref.denom & 1) && (ref.denom != 1) && !((int)ref.numer & 1))
		{
			ref.denom >>= 1;
			ref.numer >>= 1;
		}
	}

	return ref.numer * (b - a) / ref.denom;
#elif defined(WINDOWS)
	static hrtime_t ref = 0;
	if (ref == 0)
		QueryPerformanceFrequency(&ref);

	return (b - a) / ref;
#else
	return b - a;
#endif
}

hrtime_t sum_hrtime(hrtime_t a, hrtime_t b)
{
#if defined(MACOS)
	static mach_timebase_info_data_t ref;
	if (ref.denom == 0)
	{
		mach_absolute_info(&ref);

		while (((int)ref.denom & 1) && (ref.denom != 1) && !((int)ref.numer & 1))
		{
			ref.denom >>= 1;
			ref.numer >>= 1;
		}
	}

	return ref.numer * (b + a) / ref.denom;
#elif defined(WINDOWS)
	static hrtime_t ref = 0;
	if (ref == 0)
		QueryPerformanceFrequency(&ref);

	return (b + a) / ref;
#else
	return b + a;
#endif
}
 */
