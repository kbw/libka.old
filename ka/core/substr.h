#pragma once

#include <stddef.h>		// ptrdiff_t

#ifndef NO_INL_SUBSTR
#define INL_SUBSTR inline
#else
#define INL_SUBSTR
#endif

#ifdef __cplusplus
extern "C"
{
#endif

struct substr
{
	const char *begin, *end;
};

INL_SUBSTR	const char* ptr_rebase(const char* p, ptrdiff_t delta);
INL_SUBSTR	int  iswhitespace(char c);
INL_SUBSTR	int  char_in_str(char c, const char* delimiter);

INL_SUBSTR	void substr_init(struct substr *p, const char* begin, const char* end);
INL_SUBSTR	void substr_rebase(struct substr *p, const char* old, const char* _new);
INL_SUBSTR	void substr_rebase_apply_delta(struct substr *p, ptrdiff_t delta);
INL_SUBSTR	int  substr_empty(const struct substr* p);
INL_SUBSTR	size_t substr_len(const struct substr* p);
INL_SUBSTR	int  substr_cmp(const struct substr *p, const struct substr *q);
INL_SUBSTR	int  substr_ncmp(const struct substr *p, const struct substr *q, size_t n);

long long substr_strtoll(const struct substr* s, char**e, int base);

#include "substr.inl"

#ifdef __cplusplus
}
#endif
