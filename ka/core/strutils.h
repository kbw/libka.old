#ifndef KA_CORE_STRUTIL_H
#define	KA_CORE_STRUTIL_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stddef.h>

typedef void escape_callback_t(
                char* out, size_t outsz,
                const char* in, size_t sz,
                void* ctx);

escape_callback_t* set_escape_handler(escape_callback_t* func, void* ctx);

void format(char* out, size_t outsz, const char*fmt, ...) __attribute((format(printf, 3, 4)));

#ifdef __cplusplus
}
#endif

#endif	// KA_CORE_STRUTIL_H
