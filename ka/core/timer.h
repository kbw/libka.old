#pragma once

//#ifndef NO_INL_TIMER
#define INL_TIMER inline
//#else
//#define	INL_TIMER
//#endif

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __APPLE__
#define MACOS	1
#endif

#ifdef __FreeBSD__
#define FREEBSD	1
#endif

#ifdef __linux__
#define LINUX	1
#endif

#if defined(WINDOWS)
  #include <windows.h>
  typedef LARGE_INTEGER	hrtime_t;
#else
  #include <stdint.h>
  typedef	uint64_t	hrtime_t;
#endif

INL_TIMER	hrtime_t	zero_hrtime(void);
INL_TIMER	hrtime_t	get_hrtime(void);
INL_TIMER	hrtime_t	diff_hrtime(hrtime_t a, hrtime_t b);
INL_TIMER	hrtime_t	sum_hrtime(hrtime_t a, hrtime_t b);

#ifdef INL_TIMER
#include "timer.inl"
#endif

#ifdef __cplusplus
}
#endif
