#pragma once

namespace ka
{
	namespace core
	{
		inline strref::strref(const char* b, const char* e)
		{
			substr_init(&s, b, e);
		}

		inline strref::strref(const struct substr& n)
		{
			substr_init(&s, n.begin, n.end);
		}

		inline strref::~strref()
		{
		}

		inline const char*	strref::begin() const
		{
			return s.begin;
		}

		inline const char*	strref::end() const
		{
			return s.end;
		}

		inline void strref::assign(const char* b, const char* e)
		{
			substr_init(&s, b, e);
		}

		inline void strref::rebase(const char* o, const char* n)
		{
			substr_rebase(&s, o, n);
		}

		inline void strref::rebase(ptrdiff_t d)
		{
			substr_rebase_apply_delta(&s, d);
		}

		inline bool strref::empty() const
		{
			return substr_empty(&s) == 1;
		}

		inline size_t strref::len() const
		{
			return substr_len(&s);
		}

		inline int strref::cmp(const strref& b) const
		{
			return substr_cmp(&s, &b.s);
		}

		inline int strref::cmpn(const strref& b, size_t n) const
		{
			return substr_ncmp(&s, &b.s, n);
		}

		inline long long strref::strtoll(char** end, int base) const
		{
			return substr_strtoll(&s, end, base);
		}
	}
}
