#pragma once

namespace ka
{
	namespace core
	{
		inline void rcstr::set(size_t i, const value_type& s)
		{
			strs[i] = s;
		}
		
		inline void rcstr::add(const value_type& s)
		{
			strs.push_back(s);
		}
		
		inline void rcstr::push_back(const value_type& s)
		{
			strs.push_back(s);
		}
		
		inline rcstr::const_iterator rcstr::begin() const
		{
			return strs.begin();
		}
		
		inline rcstr::const_iterator rcstr::end() const
		{
			return strs.end();
		}
			
		inline const rcstr::value_type& rcstr::operator[](size_t i) const
		{
			return strs[i];
		}

		inline rcstr::value_type& rcstr::operator[](size_t i)
		{
			return strs[i];
		}

		inline bool rcstr::empty() const
		{
			return strs.empty();
		}

		inline size_t rcstr::size() const
		{
			return strs.size();
		}
			
		inline void rcstr::swap(std::string& s)
		{
			size_t len = buf.size();
			buf.push_back(
				std::shared_ptr<std::string>(new std::string));
			buf[len].get()->swap(s);
		}
	}
}
