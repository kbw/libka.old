#pragma once

#include <string.h>

inline const char* ptr_rebase(const char* p, ptrdiff_t delta)
{
	if (p)
		p += delta;

	return p;
}

inline int iswhitespace(char c)
{
	return c == ' ' || c == '\t' || c == '\r' || c == '\n';
}

inline int char_in_str(char c, const char* delimiter)
{
	return delimiter ? (strchr(delimiter, c) != NULL) : 0;
}

inline void substr_init(struct substr *p, const char* begin, const char* end)
{
	p->begin = begin;
	p->end = end;
}

inline void substr_rebase(struct substr *p, const char* old, const char* _new)
{
	ptrdiff_t delta = _new - old;

	substr_rebase_apply_delta(p, delta);
}

inline void substr_rebase_apply_delta(struct substr* p, ptrdiff_t delta)
{
	p->begin = ptr_rebase(p->begin, delta);
	p->end = ptr_rebase(p->end, delta);
}

inline int substr_empty(const struct substr* p)
{
	return p->end == p->begin;
}

inline size_t substr_len(const struct substr* p)
{
	return (size_t)(p->end - p->begin);
}

inline int substr_cmp(const struct substr *p, const struct substr *q)
{
	ptrdiff_t plen = p->end - p->begin;
	ptrdiff_t qlen = q->end - q->begin;

	if (plen < qlen)
		return -1;
	else if (plen > qlen)
		return 1;

	return strncasecmp(p->begin, q->begin, (size_t)(p->end - p->begin));
}

inline int substr_ncmp(const struct substr *p, const struct substr *q, size_t len)
{
	int cmp = strncasecmp(p->begin, q->begin, len);
//	printf("substr_len: p=%.*s q=%.*s, len=%d, cmp=%d\n", substr_len(p), p->begin, substr_len(q), q->begin, len, cmp);
	return cmp;
}
