#pragma once

#include "timer.h"
#include <iosfwd>

namespace ka
{
	namespace core
	{
		class HRTime
		{
		public:
			HRTime() :
				m_t(zero_hrtime())
			{
			}

			HRTime(hrtime_t t) :
				m_t(t)
			{
			}

			operator hrtime_t()	{ return m_t; }

			HRTime& operator+=(const HRTime& t) { m_t = m_t + t; return *this; }
			HRTime& operator-=(const HRTime& t) { m_t = m_t - t; return *this; }

			static HRTime now() { return HRTime(get_hrtime()); }

		private:
			hrtime_t m_t;

		friend  HRTime operator+(const HRTime&, const HRTime&);
		friend  HRTime operator-(const HRTime&, const HRTime&);

//		friend  std::istream& operator>>(std::istream& is, HRTime& t);
		friend  std::ostream& operator<<(std::ostream& os, const HRTime& t);

		friend  std::istream& loadFrom(std::istream& is, HRTime& t);
		friend  std::ostream& saveOn(std::ostream& os, const HRTime& t);
		};

		inline HRTime operator+(const HRTime& t, const HRTime& n)
		{
			return sum_hrtime(t.m_t, n.m_t);
		}

		inline HRTime operator-(const HRTime& t, const HRTime& n)
		{
			return diff_hrtime(t.m_t, n.m_t);
		}

		class HRTimer
		{
		public:
			HRTimer() :
				m_start(HRTime::now())
			{
			}

			void start()
			{
				m_start = HRTime::now();
			}

			void stop()
			{
				m_stop = HRTime::now();
			}

			operator hrtime_t()	{ return m_stop - m_start; }

			HRTime delta()
			{
				stop();
				return m_stop - m_start;
			}

			HRTime reset()
			{
				stop();
				HRTime dt = delta();
				m_start = m_stop = HRTime::now();
				return dt;
			}

		private:
			HRTime m_start, m_stop;

//		friend  std::istream& operator>>(std::istream& is, HRTimer& t);
		friend  std::ostream& operator<<(std::ostream& os, const HRTimer& t);

		friend  std::istream& loadFrom(std::istream& is, HRTimer& t);
		friend  std::ostream& saveOn(std::ostream& os, const HRTimer& t);
		};
	}
}
