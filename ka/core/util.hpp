#pragma once

#include "defs.hpp"

#ifndef NDEBUG
#define ka_assert(expr)		((expr) ? (void)0 : ka::assert_impl(__PRETTY_FUNCTION__, __FILE__, __LINE__, #expr))
#define ka_check(expr, msg)	((expr) ? (void)0 : ka::check_impl(__PRETTY_FUNCTION__, __FILE__, __LINE__, #expr, msg))
#else
#define ka_assert(expr)		(void)0
#define ka_check(expr, msg)	(void)0
#endif

namespace ka
{
	basic_string format(const char* fmt, ...) __attribute__((format(printf, 1, 2)));

	void assert_impl(const char *func, const char *file, int line, const char *failedexpr);
	void check_impl(const char *func, const char *file, int line, const char *failedexpr, const char* msg = nullptr);
	void check_impl(const char *func, const char *file, int line, const char *failedexpr, const std::string& msg);
}
