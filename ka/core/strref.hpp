#pragma once

#include "substr.h"

namespace ka
{
	namespace core
	{
		// String Reference
		class strref
		{
			struct substr s;

		public:
			inline strref(const char* b = nullptr, const char* e = nullptr);
			inline strref(const struct substr& n);
			inline ~strref();

			inline const char*	begin() const;
			inline const char*	end() const;

			inline void		assign(const char* b, const char* e);
			inline void		rebase(const char* o, const char* n);
			inline void		rebase(ptrdiff_t d);
			inline bool		empty() const;
			inline size_t	len() const;
			inline int		cmp(const strref& b) const;
			inline int		cmpn(const strref& b, size_t n) const;
			inline long long strtoll(char** end, int base) const;
		};
	}
}

#include "strref.inl"
