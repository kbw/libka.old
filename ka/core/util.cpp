#include "util.hpp"
#include <stdexcept>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory>

namespace
{
	ka::basic_string fail_string(const char *func, const char *file, int line, const char *failedexpr)
	{
		ka::basic_string s = func
			? ka::format("function %s, file %s, line %d", func, file, line)
			: ka::format("file %s, line %d", file, line);

		return failedexpr
			? ka::format("(%s), %s", failedexpr, s.c_str())
			: s;
	}
}

namespace ka
{
	basic_string format(const char* fmt, ...)
	{
		va_list args;
		char* raw_buf = nullptr;
		va_start(args, fmt);
		int len = vasprintf(&raw_buf, fmt, args);
		std::unique_ptr<char, decltype(&free)> buf(raw_buf, &free);
		va_end(args);

		if (!buf.get())
			throw std::bad_alloc();

		if (len < 0)
			throw std::runtime_error(basic_string("couldn't format: ") + fmt);

		return basic_string(buf.get(), (size_t)len);
	}

	void assert_impl(const char *func, const char *file, int line, const char *failedexpr)
	{
		basic_string s = fail_string(func, file, line, failedexpr);
		std::cerr << "\nAssertion failed: " << s << '\n';
		abort();
	}

	void check_impl(const char *func, const char *file, int line, const char *failedexpr, const char* msg)
	{
		std::string s = format("%s", fail_string(func, file, line, failedexpr).c_str());
		if (msg)
			s = msg + s;
		throw std::runtime_error(s);
	}

	void check_impl(const char *func, const char *file, int line, const char *failedexpr, const std::string& msg)
	{
		std::string s = format("%s: %s", msg.c_str(), fail_string(func, file, line, failedexpr).c_str());
		throw std::runtime_error(s);
	}
}
