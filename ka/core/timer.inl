#include "timer.h"

#if defined(MACOS)
  #include <mach/mach.h>
  #include <mach/mach_time.h>
#else
  #if defined(LINUX)
    #include <sys/time.h>
  #endif

  #include <time.h>
#endif

#include <stdlib.h>			// memset on Unix
#include <string.h>			// memset on Linux

#define	TIME_SEC	1
#define	TIME_MSEC	(TIME_SEC * 1000)
#define	TIME_USEC	(TIME_MSEC * 1000)
#define	TIME_NSEC	(TIME_USEC * 1000)

inline hrtime_t zero_hrtime(void)
{
#if defined(WINDOWS)
	LARGE_INTEGER val;
	memset(&val, 0, sizeof(val));
	return val;
#else
	return 0;
#endif
}

inline hrtime_t get_hrtime(void)
{
#if defined(MACOS)
	return mach_absolute_time();
#elif defined(WINDOWS)
	LARGE_INTEGER val;
	QueryPerformanceCounter(&val);
	return val;
#else
	struct timespec tv;
	clock_gettime(CLOCK_MONOTONIC, &tv);
	return (hrtime_t)(TIME_NSEC*tv.tv_sec + tv.tv_nsec);
#endif
}

inline hrtime_t diff_hrtime(hrtime_t a, hrtime_t b)
{
#if defined(MACOS)
	static mach_timebase_info_data_t ref;
	if (ref.denom == 0)
	{
		mach_absolute_info(&ref);

		while (((int)ref.denom & 1) && (ref.denom != 1) && !((int)ref.numer & 1))
		{
			ref.denom >>= 1;
			ref.numer >>= 1;
		}
	}

	return ref.numer * (a - b) / ref.denom;
#elif defined(WINDOWS)
	static hrtime_t ref = 0;
	if (ref == 0)
		QueryPerformanceFrequency(&ref);

	return (a - b) / ref;
#else
	return a - b;
#endif
}

inline hrtime_t sum_hrtime(hrtime_t a, hrtime_t b)
{
#if defined(MACOS)
	static mach_timebase_info_data_t ref;
	if (ref.denom == 0)
	{
		mach_absolute_info(&ref);

		while (((int)ref.denom & 1) && (ref.denom != 1) && !((int)ref.numer & 1))
		{
			ref.denom >>= 1;
			ref.numer >>= 1;
		}
	}

	return ref.numer * (a + b) / ref.denom;
#elif defined(WINDOWS)
	static hrtime_t ref = 0;
	if (ref == 0)
		QueryPerformanceFrequency(&ref);

	return (a + b) / ref;
#else
	return a + b;
#endif
}
