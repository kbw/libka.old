#!/bin/sh

PLATFORM=`uname`

if [ ! -z ${PLATFORM} ] && [ ${PLATFORM} == "FreeBSD" ]; then
	DEFS="-D __FreeBSD__=1 -D _LIBCPP_HAS_THREAD_API_PTHREAD=1 -D __ELF__=1"
	INCS="-I /usr/include/c++/v1 -I /usr/include -I /usr/local/include -I /home/kbw/Code/git/libka"
	IGNORE="__FreeBSD__"
fi

cppcheck -v --force --platform=unix64 --enable=all ${DEFS} ${INCS} $1 $2 $3 $4 $5 $6 $7 $8 $9 2>/dev/null | grep -v __ELF__
